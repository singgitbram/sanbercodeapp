import React, { useEffect } from 'react';
import { StyleSheet, Alert } from 'react-native';
import Tugas1 from './src/screens/Tugas1/Intro'
import Tugas2 from './src/screens/Tugas2/Biodata'
import Tugas3 from './src/screens/Tugas3/TodoList'
import Tugas4 from './src/screens/Tugas4/index'
import Tugas5 from './src/screens/Tugas5/Navigation'
import firebase from '@react-native-firebase/app'
import OneSignal from 'react-native-onesignal';
import codePush from "react-native-code-push";

// code-push release-react singgitsanbercodeapp android --> release react native updateDialog
// code-push development List singgitsanbercodeapp --> melihat history dan memanage update aplikasi
// code-push promote singgitsanbercodeapp Staging Production --> push dari staging ke production

var firebaseConfig = {
  apiKey: "AIzaSyAz2fZRBEn5R9NlvpetFMgaIjg7bonnBj0",
  authDomain: "react-native-lanjutan-auth.firebaseapp.com",
  databaseURL: "https://react-native-lanjutan-auth.firebaseio.com",
  projectId: "react-native-lanjutan-auth",
  storageBucket: "react-native-lanjutan-auth.appspot.com",
  messagingSenderId: "577473371365",
  appId: "1:577473371365:web:8bd79c346c30e3711a880c",
  measurementId: "G-MG3W3J1F43"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {

  useEffect(() => {
    //untuk push notification
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("3642635b-05a5-49f0-bf7b-230bf96165e8", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived)
    OneSignal.addEventListener('opened', onOpened)
    OneSignal.addEventListener('ids', onIds)
    
    //untuk update kl ada patch baru
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE
    }, SyncStatus)

    return () => {
      OneSignal.removeEventListener('received', onReceived)
      OneSignal.removeEventListener('opened', onOpened)
      OneSignal.removeEventListener('ids', onIds)
    }
  }, [])

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log("Checking for Update")
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log("Downloading Package")
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log("Up to Date")
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log("Installing Update")
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert("Notification", "Update Installed")
        break;
      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log("Awaiting User")
        break;
      default:
        break;
    }
  }

  const onReceived = (notification) => {
    console.log("onReceived -> notification", notification)
  }

  const onOpened = (openResult) => {
    console.log("onOpened -> openResult", openResult)
  }

  const onIds = (device) => {
    console.log("onIds -> device", device)
  }

  return (
    <Tugas5 />
  );
}
