import React, { useContext } from 'react';
import {
    StyleSheet, Text, View, TouchableOpacity, TextInput, Dimensions, FlatList
} from 'react-native';
import TodoCard from './TodoCard'
import { RootContext } from '../Tugas4'
const { height, width } = Dimensions.get('window')

export default function TodoList() {

    const state = useContext(RootContext)

    return (
        <View style={styles.container}>
            <View style={{
                alignContent: "flex-start",
                justifyContent: "flex-start",
                flexDirection: "row",
                display: "flex",
                width: width * 0.9,
                marginTop: 20
            }}>
                <Text>Masukkan Todolist</Text>
            </View>
            <View style={styles.inputBar}>
                <TextInput value={state.input} placeholder="Input here" onChangeText={(value) => state.handleChangeInput(value)} style={{ flex: 1, borderWidth: 1 }}></TextInput>
                <TouchableOpacity onPress={() => state.addTodo()} style={{ marginLeft: 5, backgroundColor: "#3EC6FF", height: 50, width: 50, justifyContent: "center", alignItems: "center", }}>
                    <Text style={{ fontSize: 30 }}>+</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={state.todos}
                renderItem={({ item, index }) => <TodoCard todo={item} idx={index} />}
                keyExtractor={(key, index) => index.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        display: 'flex',
    },
    inputBar: {
        width: width * 0.95,
        display: 'flex',
        flexDirection: 'row',
        marginTop: 10,
        height: 50,
        paddingHorizontal: 10,
        marginBottom: 10
    },
});
