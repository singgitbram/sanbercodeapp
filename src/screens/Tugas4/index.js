import React, { useState, createContext } from 'react';
import TodoList from './TodoList';
import TodoCard from './TodoCard'

export const RootContext = createContext();

const Context = () => {

    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])

    const handleChangeInput = (value) => {
        setInput(value)
    }

    const addTodo = () => {
        if (input !== '') {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            setTodos([...todos, { kerja: input, waktu: date }])
            setInput('')
        }
    }

    const deleteTodo = (indexhapus) => {
        let newTodos = [...todos]
        newTodos.splice(indexhapus, 1)
        setTodos(newTodos);
    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            deleteTodo
        }}>
            <TodoList />
            <TodoCard />
        </RootContext.Provider>
    )
}

export default Context