import React, { useContext } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { RootContext } from '../Tugas4'

const { height, width } = Dimensions.get('window')

export default function TodoCard(props) {

    const state = useContext(RootContext)

    if (props.todo) {
        return (
            < View style={styles.todoContainer}>
                <View style={{ display: "flex", marginLeft: 10, paddingVertical: 15 }}>
                    <Text>{props.todo.kerja}</Text>
                    <Text>{props.todo.waktu}</Text>
                </View>
                <View style={{ marginRight: 10 }}>
                    <TouchableOpacity onPress={() => state.deleteTodo(props.idx)}>
                        <Icon name="trash-can-outline" size={30} ></Icon>
                    </TouchableOpacity>
                </View>
            </View>
        )
    } else {
        return (
            <View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    todoContainer: {
        width: 0.9 * width,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: "white",
        borderRadius: 5,
        justifyContent: "space-between",
        borderWidth: 4,
        borderColor: "grey",
        alignItems: 'center',
        marginVertical: 5
    }
})