import React from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const { height, width } = Dimensions.get('window')

export default function TodoCard(props) {

    return (
        < View style={styles.todoContainer}>
            <View style={{ display: "flex", marginLeft: 10, paddingVertical: 15 }}>
                <Text>{props.todo.waktu}</Text>
                <Text>{props.todo.kerja}</Text>
            </View>
            <View style={{ marginRight: 10 }}>
                <TouchableOpacity onPress={() => props.passedFunction(props.idx)}>
                    <Icon name="trash-can-outline" size={30} ></Icon>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    todoContainer: {
        width: 0.9 * width,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: "white",
        borderRadius: 5,
        justifyContent: "space-between",
        borderWidth: 4,
        borderColor: "grey",
        alignItems: 'center',
        marginVertical: 5
    }
})