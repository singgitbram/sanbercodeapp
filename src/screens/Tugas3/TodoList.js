import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Dimensions,
    FlatList
} from 'react-native';
import TodoCard from './TodoCard'
const { height, width } = Dimensions.get('window')

export default function TodoList() {

    const [todos, setTodos] = useState([])
    const [text, setText] = useState('')

    function tambahtodo() {
        if (text !== '') {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            setTodos(state => [...state, { kerja: text, waktu: date }])
            setText('')
        }
    }

    function hapustodo(indexhapus) {
        let newTodos = [...todos]
        newTodos.splice(indexhapus, 1)
        setTodos(newTodos);
    }

    return (
        <View style={styles.container}>
            <View style={{
                alignContent: "flex-start",
                justifyContent: "flex-start",
                flexDirection: "row",
                display: "flex",
                width: width * 0.9,
                marginTop: 20
            }}>
                <Text>Masukkan Todolist</Text>
            </View>
            <View style={styles.inputBar}>
                <TextInput value={text} placeholder="Input here" onChangeText={(text) => setText(text)} style={{ flex: 1, borderWidth: 1 }}></TextInput>
                <TouchableOpacity onPress={() => tambahtodo()} style={{ marginLeft: 5, backgroundColor: "#3EC6FF", height: 50, width: 50, justifyContent: "center", alignItems: "center", }}>
                    <Text style={{ fontSize: 30 }}>+</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={todos}
                renderItem={({ item, index }) => <TodoCard todo={item} idx={index} passedFunction={(xx) => hapustodo(xx)} />}
                keyExtractor={(key, index) => index.toString()}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        display: 'flex',
    },
    inputBar: {
        width: width * 0.95,
        display: 'flex',
        flexDirection: 'row',
        marginTop: 10,
        height: 50,
        paddingHorizontal: 10,
        marginBottom: 10
    },
});
