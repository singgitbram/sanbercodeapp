import React from 'react';

import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export default function Intro() {

    return (
        <View style={styles.container}>
            <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
});
