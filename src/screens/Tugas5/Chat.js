import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { GiftedChat } from 'react-native-gifted-chat'
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth'

export default function Chat() {

    const [messages, setMessages] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const user = auth().currentUser
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messages')
            if (db) {
                db.off()
            }
        }
    }, [])

    const getData = () => {
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages, value))
        })
    }

    const onSend = (messages = []) => {
        for (let i = 0; i < messages.length; i++) {
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
    }

    return (
        <GiftedChat
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: "https://store.playstation.com/store/api/chihiro/00_09_000/container/US/en/99/UP1675-CUSA11816_00-AV00000000000012//image?_version=00_09_000&platform=chihiro&w=720&h=720&bg_color=000000&opacity=100"
            }}
        />
    )
}