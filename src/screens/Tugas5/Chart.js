import React, { useState, useEffect } from "react";
import { View, Text, processColor } from "react-native";
import { BarChart } from "react-native-charts-wrapper";

// const data = [
// { y: [100, 40], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [80, 60], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [40, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [78, 45], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [67, 87], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [98, 32], marker: ["React Native Dasar", "React Native Lanjutan"] },
// { y: [150, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
// ]

const data = [
    [100, 40],
    [80, 60],
    [40, 90],
    [78, 45],
    [67, 87],
    [98, 32],
    [150, 90],
]

const dataBaru = []

export default function Chart(route) {
    // console.log("Chart -> route", route.params.name)

    // const transformChart = () => {
    //     let temp = []
    //     data.forEach(Element => temp.push({ y: Element, marker: [`React Native Dasar ${[Element[0]]}`, `React Native Lanjutan ${[Element[1]]}`] }))
    //     setDataChart(temp)
    // }

    // const transformChart2 = () => {
    //     setChart(
    //         {
    //             data: {
    //                 dataSets: [{
    //                     values: dataChart,
    //                     label: '',
    //                     config: {
    //                         colors: [processColor('lightskyblue'), processColor('dodgerblue')],
    //                         stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
    //                         drawFilled: false,
    //                         drawValues: false,
    //                     }
    //                 }]
    //             }
    //         })
    // }

    // useEffect(() => {
    //     setTimeout(() => {
    //         transformChart()
    //         transformChart2()
    //     }, 3000)
    // }, [])

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: dataBaru,
                label: '',
                config: {
                    colors: [processColor('lightskyblue'), processColor('dodgerblue')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })

    if (dataBaru.length == 0) {
        data.map((datum, i) => {
            dataBaru.push({ y: datum, marker: [`React Native Dasar \n ${[datum[0]]}`, `React Native Lanjutan \n ${[datum[1]]}`] })
        })
    }

    return (
        <View style={{ flex: 1 }}>
            <BarChart
                style={{ flex: 1 }}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                chartDescription={{ text: '' }}
                doubleTapToZoomEnabled={false}
                pinchZoom={false}
                legend={legend}
                marker={{ enabled: true, markerColor: processColor('grey'), textColor: processColor('white'), textSize: 14 }}
            />
        </View>
    )


}