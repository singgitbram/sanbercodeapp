import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    StatusBar,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    Alert
} from 'react-native';
const { height, width } = Dimensions.get('window')
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import api from '../../api'
import auth from '@react-native-firebase/auth'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'
import { CommonActions } from '@react-navigation/native';

const fingerprintConfig = {
    title: 'Authentication Required',
    imageColor: "#191970",
    imageErrorColor: "red",
    sensorDescription: 'Touch sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem("token", token)
        } catch (err) {
            console.log(err)
        }
    }



    useEffect(() => {
        Asyncstorage.setItem("skipIntro", 'true')
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '577473371365-ue1cab9mkv8bntbablpa4arvd8fdsa8s.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            // ini supaya google sign in integrasi dengan firebase 
            const credential = auth.GoogleAuthProvider.credential(idToken)
            auth().signInWithCredential(credential)
            navigation.navigate('Utama')

        } catch (error) {
            Alert.alert(
                'Something Went Wrong',
                'Please try again',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') }
                ],
                { cancelable: false }
            );
        }
    }

    const onRegisterPress = () => {
        navigation.navigate('Register')
    }

    // const onLoginPress = () => {
    //     return auth().signInWithEmailAndPassword(email, password)
    //         .then((res) => {
    //             navigation.navigate('Utama')
    //         })
    //         .catch((err) => {
    //             Alert.alert(
    //                 'Wrong Email/Password',
    //                 'Please try again',
    //                 [
    //                     { text: 'OK', onPress: () => console.log('OK Pressed') }
    //                 ],
    //                 { cancelable: false }
    //             );
    //         })
    // }

    const onLoginPress = () => {
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${api}/login`, data)
            .then(res => {
                saveToken(res.data.token)

                // navigation.navigate('Utama')

                // navigation.dispatch(
                //     CommonActions.reset({
                //         index: 0,
                //         routes: [{ name: 'Utama' }]
                //     })
                // )

                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Utama' }]
                })
            })
            .catch(err => {
                Alert.alert(
                    'Wrong Email/Password',
                    'Please try again',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') }
                    ],
                    { cancelable: false }
                );
            })
    }

    const signInWithFingerprint = () => {
        TouchID.authenticate('', fingerprintConfig)
            .then(success => {
                navigation.navigate('Utama')
            })
            .catch(error => {
                alert("Authentication Failed")
            })
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <Image source={require('../../assets/images/logo.jpg')} style={{ width: width, height: 200 }} />
            <Text style={{ alignSelf: 'flex-start', marginLeft: 20, fontWeight: 'bold', fontSize: 20 }}>Username</Text>
            <View style={styles.inputText}>
                <TextInput value={email} autoCapitalize="none" onChangeText={(email) => setEmail(email)} placeholder="Username or Email" style={{ fontSize: 13 }}></TextInput>
            </View>
            <Text style={{ alignSelf: 'flex-start', marginLeft: 20, fontWeight: 'bold', marginTop: 10, fontSize: 20 }}>Password</Text>
            <View style={styles.inputText}>
                <TextInput secureTextEntry={true} autoCapitalize="none" value={password} onChangeText={(password) => setPassword(password)} placeholder="Password" style={{ fontSize: 13 }}></TextInput>
            </View>
            <TouchableOpacity onPress={() => onLoginPress()} style={styles.buttonMasuk}>
                <Text style={{ backgroundColor: "#3EC6FF", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 5, width: width * 0.9, textAlign: 'center' }}>LOGIN</Text>
            </TouchableOpacity>
            <View style={{ marginTop: 10, marginBottom: 5 }}>
                <Text style={{ textAlign: 'center' }}>─────────  OR  ─────────</Text>
            </View>
            <View style={{ marginTop: 5 }}>
                <GoogleSigninButton
                    onPress={() => signInWithGoogle()}
                    style={{ width: 330, height: 40 }}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                />
            </View>
            <TouchableOpacity onPress={() => signInWithFingerprint()} style={styles.buttonMasuk}>
                <Text style={{ backgroundColor: "#191970", color: "white", paddingHorizontal: 25, paddingVertical: 5, borderRadius: 5, width: width * 0.9, textAlign: 'center' }}>SIGN IN WITH FINGERPRINT</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => onRegisterPress()} style={{ marginTop: 'auto' }}>
                <Text style={{ marginBottom: 20 }}>Belum mempunyai akun ? <Text style={{ color: "blue" }}>Buat Akun</Text></Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        display: 'flex',
    },
    inputText: {
        width: width * 0.9,
        borderBottomWidth: 1,
        borderColor: "#aaa69d",
        flexDirection: "row",
    },
    buttonMasuk: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
    },
    buttonKeGoogle: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    }
});

export default Login;