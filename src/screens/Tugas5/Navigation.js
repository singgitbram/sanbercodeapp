import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import SplashScreen from './SplashScreen';
import Intro from './Intro';
import Login from './Login';
import Biodata from '../Tugas2/Biodata'
import Register from './Register'
import Homepage from './Homepage'
import Maps from "./Maps";
import Chart from "./Chart";
import Chat from "./Chat";
import Asyncstorage from '@react-native-community/async-storage'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Entypo from 'react-native-vector-icons/Entypo'

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator()

const TabsNavigation = () => (
    <Tabs.Navigator
        initialRouteName="Biodata">
        <Tabs.Screen name="Homepage" component={Homepage} options={{
            title: 'Home', tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
            ),
        }} />
        <Tabs.Screen name="Maps" component={Maps} options={{
            title: 'Maps', tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="map-marker" color={color} size={size} />
            ),
        }} />
        <Tabs.Screen name="Chat" component={Chat} options={{
            title: 'Chat', tabBarIcon: ({ color, size }) => (
                <Entypo name="chat" color={color} size={size} />
            ),
        }} />
        <Tabs.Screen name="Biodata" component={Biodata} options={{
            title: 'Profile', tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="account-box" color={color} size={size} />
            ),
        }} />
    </Tabs.Navigator>
)

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Utama" component={TabsNavigation} options={{ headerShown: false }} />
        <Stack.Screen name="Chart" component={Chart} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
    </Stack.Navigator>
)

function AppNavigation() {
    const [isLoading, setIsLoading] = React.useState(true)
    const [skip, setSkip] = React.useState('')
    const [isLogin, setIsLogin] = React.useState('')
    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        async function getSkip() {
            try {
                const skipStatus = await Asyncstorage.getItem("skipIntro")
                setSkip(skipStatus)
            } catch (err) {
                console.log(err)
            }
        }
        async function getLogin() {
            try {
                const loginStatus = await Asyncstorage.getItem("token")
                if (loginStatus) {
                    setIsLogin('true')
                }
            } catch (err) {
                console.log(err)
            }
        }
        getSkip()
        getLogin()
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            {skip == 'true' ? (
                <Stack.Navigator initialRouteName={isLogin == 'true' ? "Utama" : "Login"}>
                    <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
                    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                    <Stack.Screen name="Utama" component={TabsNavigation} options={{ headerShown: false }} />
                    <Stack.Screen name="Chart" component={Chart} options={{ headerShown: false }} />
                    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                </Stack.Navigator>
            ) : (
                    <Stack.Navigator initialRouteName="Intro">
                        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
                        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                        <Stack.Screen name="Utama" component={TabsNavigation} options={{ headerShown: false }} />
                        <Stack.Screen name="Chart" component={Chart} options={{ headerShown: false }} />
                        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                    </Stack.Navigator>
                )
            }
        </NavigationContainer>
    )
}

export default AppNavigation;