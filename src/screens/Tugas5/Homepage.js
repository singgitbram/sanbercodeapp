import React from 'react';
import { View, Image, StyleSheet, Dimensions, Text, StatusBar, ScrollView, TouchableOpacity } from "react-native";
const { height, width } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

function Homepage({ navigation }) {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <View style={styles.kelasContainer}>
                <View style={styles.containerTitle}>
                    <Text style={{ color: 'white', paddingLeft: 10 }}>Kelas</Text>
                </View>
                <View style={styles.kelasContent}>
                    <TouchableOpacity onPress={() => { navigation.navigate('Chart', { name: 'bramz' }) }}>
                        <View style={styles.contentIcon}>
                            <Icon name="logo-react" size={45} color={'white'} />
                            <Text style={styles.iconDescription}>React Native</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.contentIcon}>
                        <Icon name="logo-python" size={45} color={'white'} />
                        <Text style={styles.iconDescription}>Data Science</Text>
                    </View>
                    <View style={styles.contentIcon}>
                        <Icon name="logo-react" size={45} color={'white'} />
                        <Text style={styles.iconDescription}>React JS</Text>
                    </View>
                    <View style={styles.contentIcon}>
                        <Icon name="logo-laravel" size={45} color={'white'} />
                        <Text style={styles.iconDescription}>Laravel</Text>
                    </View>
                </View>
            </View>
            <View style={styles.kelasContainer}>
                <View style={styles.containerTitle}>
                    <Text style={{ color: 'white', paddingLeft: 10 }}>Kelas</Text>
                </View>
                <View style={styles.kelasContent}>
                    <View style={styles.contentIcon}>
                        <Icon name="logo-wordpress" size={45} color={'white'} />
                        <Text style={styles.iconDescription}>Wordpress</Text>
                    </View>
                    <View style={styles.contentIcon}>
                        <Image
                            style={{ width: 45, height: 45 }}
                            source={require('../../assets/icons/website-design.png')}
                        />
                        <Text style={styles.iconDescription}>Design Grafis</Text>
                    </View>
                    <View style={styles.contentIcon}>
                        <MaterialIcon name="server" size={45} color={'white'} />
                        <Text style={styles.iconDescription}>Web Server</Text>
                    </View>
                    <View style={styles.contentIcon}>
                        <Image
                            style={{ width: 45, height: 45 }}
                            source={require('../../assets/icons/ux.png')}
                        />
                        <Text style={styles.iconDescription}>UI/UX Design</Text>
                    </View>
                </View>
            </View>
            <View style={styles.summaryContainer}>
                <View style={styles.containerTitle}>
                    <Text style={{ color: 'white', paddingLeft: 10 }}>Summary</Text>
                </View>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>React Native</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>Data Science</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>React JS</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>Laravel</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>Wordpress</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>Design Grafis</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>Web Server</Text>
                    </View>
                    <View style={styles.summaryContent}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                    <View style={styles.summaryTitle}>
                        <Text style={{ color: 'white', paddingLeft: 10 }}>UI/UX Design</Text>
                    </View>
                    <View style={{
                        display: 'flex',
                        backgroundColor: "#088dc4",
                        paddingVertical: 5,
                        borderBottomRightRadius: 10,
                        borderBottomLeftRadius: 10
                    }}>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Today</Text>
                            <Text style={styles.summaryTextContent}>20 orang</Text>
                        </View>
                        <View style={styles.summaryRowContent}>
                            <Text style={styles.summaryTextContent}>Total</Text>
                            <Text style={styles.summaryTextContent}>100 orang</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
    },
    kelasContainer: {
        display: 'flex',
        width: width * 0.9,
        marginVertical: 5
    },
    containerTitle: {
        backgroundColor: "#088dc4",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        height: 30,
        justifyContent: 'center'
    },
    kelasContent: {
        backgroundColor: '#3EC6FF',
        flexDirection: 'row',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    contentIcon: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconDescription: {
        color: 'white',
        fontSize: 10
    },
    summaryContainer: {
        display: 'flex',
        width: width * 0.9,
        marginVertical: 5,
        height: 300,
    },
    scrollView: {
        display: 'flex',
    },
    summaryTitle: {
        backgroundColor: '#3EC6FF',
        height: 30,
        justifyContent: 'center'
    },
    summaryContent: {
        display: 'flex',
        backgroundColor: "#088dc4",
        paddingVertical: 5
    },
    summaryTextContent: {
        color: 'white'
    },
    summaryRowContent: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        marginVertical: 2
    }
})

export default Homepage;