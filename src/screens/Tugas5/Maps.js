import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import MapboxGL from "@react-native-mapbox-gl/maps";

MapboxGL.setAccessToken('pk.eyJ1Ijoic2luZ2dpdGJyYW0iLCJhIjoiY2tkZmoxdXZ2MWU4azJ5bXQwN2poaHZ3MiJ9.ntcYXv_7f2lrqkcP1kbyTA')

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]

export default function Maps() {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            }
            catch (error) {
                console.log(error)
            }
        }   
        getLocation()
    }, [])

    return (
        <View style={{ flex: 1 }}>
            <MapboxGL.MapView style={{ flex: 1 }}>
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />
                {coordinates.map((kordinat, i) => {
                    return (
                        <MapboxGL.PointAnnotation
                            key={i}
                            id={i.toString()}
                            coordinate={kordinat}
                        >
                            <MapboxGL.Callout
                                title={`Longitude: ${kordinat[0]} Latitude: ${kordinat[1]}`}
                            />
                        </MapboxGL.PointAnnotation>
                    )
                })}
            </MapboxGL.MapView>
        </View>
    );
}
